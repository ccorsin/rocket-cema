const rocketData = {
  "angle": 8,
  "speed": 3,
  "weight": 54,
}

const asteroidData = {
  "hardness": 4,
  "weight": 1000,
  "composition": "nickel",
}

module.exports = {
  data: rocketData,
  asteroidData: asteroidData,
};
